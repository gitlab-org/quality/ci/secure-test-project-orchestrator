# Secure Test Orchestrator

This repo contains CI configuration to execute a pipeline to kick off [Secure Test Projects](https://gitlab.com/gitlab-org/security-products/tests/) automated testing jobs

It also updates the Information table https://gitlab-org.gitlab.io/quality/ci/secure-test-project-orchestrator/ on a weekly basis.

## Stages

`dynamic_job`

Job to generate a child pipeline CI config.

This calls the API to get all `security-products/tests` projects.

Projects are matched against a CI variable `PROJECT_REGEXP`, and if they match the branches are then iterated over. If the branch matches against the CI variable `BRANCH_REGEXP` then an entry is added to the generated CI.

`trigger-tests`

This kicks off the child pipeline from the generated CI config, which itself kicks off jobs for each project and branch combination that matched the CI variables.

`notify-slack-*`

Test failures are reported to the [`s_secure-alerts`](https://gitlab.slack.com/archives/CAU9SFKNU) Slack channel

Successful executions are reported to the [`s_secure-ops`](https://gitlab.slack.com/archives/C01Q50CDWKC) Slack channel, this allows for visibility if no failures are otherwise reported

`pages` / `pages:deploy`

The pages job iterates over all of the test projects, reads the `README` tables for information on reported capabilities, and then uses the last default branch pipeline of each project to obtain details of the jobs that were executed. This information is tabulated and reported to https://gitlab-org.gitlab.io/quality/ci/secure-test-project-orchestrator/

This is only executed when `WHICH_SCHEDULE` is set to `weekly`

## CI variables

| Variable | Used for | Example |
| ------ | ------ | ------ |
| `PROJECT_REGEXP` | Match on project name | `^(?!Custom CA using Nginx$\|Gdb$\|Elixir Phoenix Umbrella$\|Common$\|python-poetry$\|ruby-advisory-db$)(.*)$` |
| `BRANCH_REGEXP` | Match on branch name | `^(develop\|master)$\|^(?!((auto-devops).)\|((QA-all-).)).*-FREEZE*$` |
| `WHICH_SCHEDULE` | Generate the [Information page](https://gitlab-org.gitlab.io/quality/ci/secure-test-project-orchestrator/) if this is `weekly` (currently set on Sunday scheduled pipeline) | `weekly` |

## Schedule
The pipeline is currently set to execute on a daily cadence at 1am UTC

On Sunday 1am the [Information page](https://gitlab-org.gitlab.io/quality/ci/secure-test-project-orchestrator/) is updated
