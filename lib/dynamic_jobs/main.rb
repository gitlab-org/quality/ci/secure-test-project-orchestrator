# frozen_string_literal: true

require_relative 'dynamic_jobs.rb'

unless ENV['BRANCH_REGEXP']
  raise 'Please ensure that the Branch Regular expression BRANCH_REGEXP is ' \
        'set as a CI variable'
end

DynamicJobs.generate_dynamic_ci
