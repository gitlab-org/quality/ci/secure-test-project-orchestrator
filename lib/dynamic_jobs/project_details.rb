# frozen_string_literal: true

# Module for getting analyzer details from projects
module ProjectDetails
  require 'httparty'
  require 'open-uri'
  require 'rdoc'
  require_relative 'dynamic_jobs'
  require 'nokogiri'
  require 'builder'

  RAW_SUFFIX = '/-/raw/master/README.md'

  def self.build_html_table(details_hash)
    test_headers = ['Scanner', 'Language', 'Framework', 'Package Manager', 'Latest Default Branch Pipeline', 'SAST', 'Dependency Scanning', 'Container Scanning', 'DAST', 'License Management', 'Code Quality']
    details_hash.first.each_value do |item|
      item.each_key do |k|
        test_headers.push(k) unless test_headers.include?(k)
      end
    end
    html = Builder::XmlMarkup.new(indent: 2)
    html.table(border: '1px solid black', id: 'test_projects') do
      html.thead do
        html.tr do
          test_headers.each { |h| html.th(h) }
        end
      end
      html.tbody do
        details_hash.first.each_key do |k|
          html.tr do
            html.td(k)
            test_headers.drop(1).each do |column|
              if column.eql?('Latest Default Branch Pipeline')
                if details_hash.first[k]
                  html.td << details_hash.first[k][column]
                end
              else
                html.td(details_hash.first[k][column]) if details_hash.first[k]
              end
            end
          end
        end
      end
    end
    html
  end

  def self.replace_emoji_strings(input_string)
    input_string = input_string.gsub(':x:', '❌')
    input_string = input_string.gsub(':whitecheckmark:', '✅')
    input_string = input_string.gsub(':heavycheckmark:', '✅')
    input_string = input_string.gsub('failed', '☠️ Failed')
    input_string = input_string.gsub('success', '🎉 Passed')
    input_string.gsub(':warning:', '⚠️')
  end

  def self.run_project_details
    project_feature_hash = parse_project_features
    html_table = build_html_table([project_feature_hash])
    table_string = replace_emoji_strings(html_table.target!)

    File.open('index.html', 'w') do |file|
      file.puts '<html><body>'
      file.puts '<script type="text/javascript" src="DataTables/jquery-3.5.1.js"></script>'
      file.puts '<script type="text/javascript" src="DataTables/DataTables-1.10.23/js/jquery.dataTables.min.js"></script>'
      file.puts '<script type="text/javascript" src="DataTables/datatables.min.js"></script>'
      file.puts '<link rel="stylesheet" href="DataTables/DataTables-1.10.23/css/jquery.dataTables.min.css">'
      file.puts '<link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>'
      file.puts table_string
      file.puts '</body><script type="text/javascript">'
      file.puts '$(document).ready( function () {'
      file.puts "$('#test_projects').DataTable("
      file.puts ' { "paging":   false });'
      file.puts '} );'
      file.puts '</script></html>'
    end

    HttpUtils.report_to_slack(
        "Updated Test Project details page at https://gitlab-org.gitlab.io/quality/ci/secure-test-project-orchestrator/",
        'security-tanuki',
        's_secure-ops'
    )
  end

  def self.parse_pipeline_jobs(project_details)
    pipelines = HTTParty.get("https://gitlab.com/api/v4/projects/#{project_details['id']}/pipelines/?ref=#{project_details['default_branch']}&order_by=id")

    output_project_hash = {}

    return output_project_hash if pipelines.empty?

    pipeline_id = pipelines.first['id']
    jobs = HTTParty.get("https://gitlab.com/api/v4/projects/#{project_details['id']}/pipelines/#{pipeline_id}/jobs")

    return output_project_hash if jobs.empty?

    jobs.each do |job|
      output_project_hash.merge!({ job['name'] => job['status'] })
    end
    output_project_hash
  end

  def self.parse_project_features
    project_feature_hash = {}
    projects = DynamicJobs.api_group_active_projects
    projects.each do |project|
      puts project['web_url']
      readme_raw_url = "#{project['web_url']}#{RAW_SUFFIX}"
      next unless HttpUtils.url_exist?(readme_raw_url) && !project['archived'] && !project['empty_repo']
      md = RDoc::Markdown.parse(URI.open(readme_raw_url).read)
      project_feature_hash[project['name']] = parse_pipeline_jobs(project)
      project_feature_hash[project['name']].merge!({ 'Latest Default Branch Pipeline' => "<img src=\"#{project['web_url']}/badges/#{project['default_branch']}/pipeline.svg\">" })
      md.each_with_index do |markup_line, index|
        if markup_line.is_a? RDoc::Markup::List
          markup_line.items.each do |item_line|
            if item_line.parts.first.text.match?('(<b>Language:<\/b>)')
              project_feature_hash[project['name']].merge!({ 'Language' =>
                                                                 item_line.parts.first.text
                                                      .match('(?:<b>Language:<\/b>\s*)(.*)')[1] })
            end
            if item_line.parts.first.text.match?('(<b>Package Manager:<\/b>)')
              project_feature_hash[project['name']].merge!({ 'Package Manager' =>
                                                                 item_line.parts.first.text
                                                                     .match('(?:<b>Package Manager:<\/b>\s*)(.*)')[1] })
            end
            unless item_line.parts.first.text.match?('(<b>Framework:<\/b>)')
              next
            end

            project_feature_hash[project['name']].merge!({ 'Framework' =>
                                                               item_line.parts.first.text
                                                                   .match('(?:<b>Framework:<\/b>\s*)(.*)')[1] })
          end
        end
      end
    end

    project_feature_hash
  end
end

ProjectDetails.run_project_details
