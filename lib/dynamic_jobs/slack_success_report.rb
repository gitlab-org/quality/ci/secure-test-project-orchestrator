# frozen_string_literal: true

# Module for reporting failed pipelines for the secure test pipelines
module SlackSuccessReport
  require_relative 'http_utils'

  SUCCESS_MESSAGE = '🎉 QA against Secure test projects passed! 🎉 See '
  CHANNEL = 's_secure-ops'

  def self.report_headline_to_slack
    HttpUtils.report_to_slack("#{SUCCESS_MESSAGE}#{ENV['CI_PIPELINE_URL']}",
                              ':ci_passing:',
                              CHANNEL)
  end

  def self.report_unmatching_projects(projects)
    report_on_projects = ''
    projects.each do |project|
      report_on_projects += "<https://gitlab.com/gitlab-org/security-products/tests/#{project}|#{project}>\n"
    end
    HttpUtils.report_to_slack(
      "Projects not ran due to non matching project regexp on Secure Test Orchestrator:\n#{report_on_projects}",
      ':info:',
      CHANNEL
    )
  end
end
