# frozen_string_literal: true

require 'rspec'
require 'webmock/rspec'
require 'dynamic_jobs/slack_failure_report'

RSpec.describe SlackFailureReport do
  context '.job_names' do
    before do
      ENV['CI_API_V4_URL'] = 'http://foo'
      @mock_child_pipeline = {
        'downstream_pipeline' => {
          'project_id' => '42',
          'id' => '24'
        }
      }
    end

    it 'returns array of failed job names given details of pipeline with failed jobs' do
      mock_jobs = [
        {
          'name' => 'job_a',
          'status' => 'passed'
        },
        {
          'name' => 'job_b',
          'status' => 'manual'
        },
        {
          'name' => 'job_c',
          'status' => 'failed'
        },
        {
          'name' => 'job_d',
          'status' => 'failed'
        }
      ]
      expect(HttpUtils).to receive(:paginated_get).with('http://foo/projects/42/pipelines/24/jobs').and_return(mock_jobs)
      expect(SlackFailureReport.job_names(@mock_child_pipeline)).to eq(%w[job_c job_d])
    end
    it 'returns empty array of failed job names given details of pipeline with no failed jobs' do
      mock_jobs = [
        {
          'name' => 'job_a',
          'status' => 'passed'
        },
        {
          'name' => 'job_b',
          'status' => 'manual'
        },
        {
          'name' => 'job_c',
          'status' => 'manual'
        },
        {
          'name' => 'job_d',
          'status' => 'passed'
        }
      ]
      expect(HttpUtils).to receive(:paginated_get).with('http://foo/projects/42/pipelines/24/jobs').and_return(mock_jobs)
      expect(SlackFailureReport.job_names(@mock_child_pipeline)).to eq([])
    end
  end
  describe '#link_url' do
    before do
      ENV['CI_API_V4_URL'] = 'http://foo'
    end
    context 'when child pipeline job has a downstream pipeline' do
      it 'returns the downstream pipeline web url' do
        child_pipeline_job = {
          'downstream_pipeline' => {
            'web_url' => 'https://example.com/pipelines/1'
          }
        }

        expect(SlackFailureReport.link_url(child_pipeline_job)).to eq('https://example.com/pipelines/1')
      end
    end

    context 'when child pipeline job does not have a downstream pipeline' do
      let(:child_pipeline_job) do
        {
          'name' => 'project-name-branch',
          'pipeline' => {
            'web_url' => 'https://example.com/pipelines/2'
          }
        }
      end

      it 'returns child pipeline web url if pipeline_url does not return a URL' do
        allow(SlackFailureReport).to receive(:pipeline_url).and_return(nil)

        expect(SlackFailureReport.link_url(child_pipeline_job)).to eq('https://example.com/pipelines/2')
      end

      it 'returns the pipeline_url if it finds a valid pipeline' do
        allow(SlackFailureReport).to receive(:pipeline_url).with('project', 'name-branch').and_return(nil)
        allow(SlackFailureReport).to receive(:pipeline_url).with('project-name', 'branch').and_return('https://found-pipeline')

        expect(SlackFailureReport.link_url(child_pipeline_job)).to eq('https://found-pipeline')
      end
    end
  end
  describe '#pipeline_url' do
    let(:project_name) { 'my-project' }
    let(:branch) { 'my-branch' }

    it 'returns nil if the URL does not exist' do
      stub_request(:get, "https://gitlab.com/gitlab-org/security-products/tests/#{project_name}/-/tree/#{branch}")
        .to_return(status: 404)

      expect(SlackFailureReport.pipeline_url(project_name, branch)).to be_nil
    end

    it 'returns the pipeline URL if it exists' do
      url = "https://gitlab.com/gitlab-org/security-products/tests/#{project_name}/-/tree/#{branch}"
      stub_request(:get, url)
        .to_return(status: 200)

      expect(SlackFailureReport.pipeline_url(project_name, branch)).to eq(url)
    end
  end

  describe '#child_bridges' do
    let(:pipeline_id) { 1 }

    context 'when pipeline has child bridges' do
      before do
        allow(HttpUtils).to receive(:paginated_get).and_return([{
                                                                 'downstream_pipeline' => {
                                                                   'id' => 2
                                                                 }
                                                               }])
      end

      it 'gets bridges for parent pipeline' do
        expect(HttpUtils).to receive(:paginated_get).with(/projects\/19722963\/pipelines\/#{pipeline_id}\/bridges/)
        described_class.child_bridges(pipeline_id)
      end

      it 'gets id of first child pipeline' do
        bridges = described_class.child_bridges(pipeline_id)
        expect(bridges.first['downstream_pipeline']['id']).to eq(2)
      end

      it 'gets bridges for child pipeline' do
        expect(HttpUtils).to receive(:paginated_get).with(/projects\/19722963\/pipelines\/2\/bridges/)
        described_class.child_bridges(pipeline_id)
      end
    end

    context 'when pipeline has no child bridges' do
      before do
        allow(HttpUtils).to receive(:paginated_get).and_return([])
      end

      it 'returns empty array' do
        expect(described_class.child_bridges(pipeline_id)).to eq([])
      end
    end
  end

  describe '#collect_previous_result' do
    let(:pipelines) { [pipeline1, pipeline2] }
    let(:pipeline1) { { 'id' => 1 } }
    let(:pipeline2) { { 'id' => 2 } }

    before do
      allow(HttpUtils).to receive(:paginated_get).and_return(pipelines)
      allow(ENV).to receive(:[]).with('CI_PIPELINE_ID').and_return('2')
    end

    it 'builds hash for each non-successful child pipeline job' do
      child_pipelines = [
        { 'status' => 'success' },
        { 'status' => 'failed', 'name' => 'pipe1' }
      ]
      allow(described_class).to receive(:child_bridges).and_return(child_pipelines)
      allow(described_class).to receive(:job_names).and_return(['job1'])

      expect(described_class.collect_previous_result).to eq([{ 'name' => 'pipe1', 'jobs' => ['job1'] }])
    end
  end
end
